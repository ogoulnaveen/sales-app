import { BaseDetail } from "./Basedetail.model";

export class UserDetail  extends BaseDetail {
    user_id: string;
    usertype_name:string;
    username: string;
    password: string;
    email: string;
    phone: string;
    first_name: string;
    last_name: string;
    files:string;

}