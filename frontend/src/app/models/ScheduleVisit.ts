import { BaseDetail } from "./Basedetail.model";

export class ScheduleVisit  extends BaseDetail {
    schedule_visit_id:number;
    vendor_id: string;
    phone:string;
    address:string;
    sales_user_id: string;
    schedule_visit_added_by_user_id: string;
    date: string;
    time: string;
    sales_email: string;
    vendor_name:string;


}