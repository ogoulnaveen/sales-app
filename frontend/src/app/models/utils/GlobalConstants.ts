/**
 * All constants used in the application will go in this class file
 */
import { environment } from "../../../environments/environment";

export class GlobalConstants {

  


    static readonly OBJECT_DETAIL_CUSTOMER = 3;
    static readonly OBJECT_DETAIL_CONTACT = 1;
    static readonly OBJECT_DETAIL_LEAD = 2;
    

    //local storage variable key names

    //read from environment file
    static readonly BACKEND_SERVER_URL = environment.BACKEND_SERVER_URL;
    static readonly SERVER_URL = environment.SERVER_URL;

    //default file locations
    static readonly DEFAULTS_FILE_LOCATIONS = {
        // ENTITY_ICONS: "assets/images/sectors/"
        // ENTITY_ICONS: "c://fakepath//"
  /*     ENTITY_ICONS: "../../backend/public/"  */
          ENTITY_ICONS:GlobalConstants.BACKEND_SERVER_URL +'/'
     
        /* ENTITY_ICONS:'public' */
    }

    //reporting types





    constructor() {
    }
}