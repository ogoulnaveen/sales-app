import { BaseDetail } from "./Basedetail.model";

export class ProductDetail  extends BaseDetail  {
    product_id: string;
    product_name: string;
    cost: number;
    supplier_name: string;
    category_name: string;  
    files:string; 
} 