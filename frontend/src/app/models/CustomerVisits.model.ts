import { BaseDetail } from "./Basedetail.model";

export class CustomerVisits  extends BaseDetail {
    visit_id: number;
    vendor_id: string;
    
    //address: string;
    /*customer_name: string;
    email: string;
   phone: string;
   first_name: string;
   last_name: string;
   
   visit_added_by_username:string;
   visit_added_by_name: string; */
  /*   visit_added_by_email: string; */
    visits_note: string;
    lat: number;
    long: number;
    user_id:string;
    
  
}