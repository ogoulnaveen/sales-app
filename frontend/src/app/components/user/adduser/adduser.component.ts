import { Component, OnInit } from '@angular/core';
import { DBService } from 'src/app/services/dbservice.service';
import { UserDetail } from 'src/app/models/UserDetail.model';
import { Router, ActivatedRoute } from '@angular/router';
import {ValidateService} from 'src/app/services/validate.service'
import { CustomLogger } from '../../../models/utils/CustomLogger';
import { CustomMisc } from '../../../models/utils/CustomMisc';
import { UserType } from 'src/app/models/UserType.model';
import { GlobalConstants } from 'src/app/models/utils/GlobalConstants';
import { FormBuilder, FormGroup } from "@angular/forms";
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import {FlashMessagesService} from 'angular2-flash-messages';
@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  userDetail: UserDetail;
  userType: UserType[] = [];
  form: FormGroup;
  file 
  message: any;
  preview: string;
  constructor(private _dbService: DBService, private _router: Router, private _activatedRoute: ActivatedRoute,public fb: FormBuilder,
    private validateService: ValidateService,  private flashMessage:FlashMessagesService,private toastyService: ToastyService,) {
     // Reactive Form
     this.form = this.fb.group({
      name: [''],
      files: [null]
    })
   }

  isUpdate = false;
  fieldTextType: boolean;



  async htmlInit() {
    
    let result = await this._dbService.getAllUserType().toPromise();
    this.userType = result["data"];
} 
catch (error) {
    CustomLogger.logStringWithObject("ERROR:", error);
}
  ngOnInit() {
    this.userDetail = new UserDetail();
    this.htmlInit();
    this._activatedRoute.params.subscribe(
      async params => {
        console.log("params:", params);
        let user_id = params["id"];
        if (user_id) {
          let result = await this._dbService.getUser(user_id).toPromise();
          console.log(result);
          this.userDetail = result["data"];
          this.isUpdate = true;
         this.preview=this.userDetail.files;
          
        }

      }
    );

  }

  showError = "";

  async onSubmit() {
   
    CustomLogger.logStringWithObject("Will save user...", this.userDetail);

    this.userDetail.updated_by= this._dbService.getCurrentUserDetail().email; 
    this.userDetail.modified_time = Date.now();
    try {
      let result = null;
      CustomLogger.logStringWithObject("FILES:", this.file);
      if (this.file != undefined || this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        result = await this._dbService.uploadFile(formData).toPromise();
        CustomLogger.logStringWithObject("uploadFile:result:", result);

        let fileName = GlobalConstants.DEFAULTS_FILE_LOCATIONS.ENTITY_ICONS + result["data"].filename;//received after file added
        CustomLogger.logStringWithObject("files:", fileName);
        this.userDetail.files = fileName;

      }
     
     
      if (this.isUpdate)
    
      result = await this._dbService.updateUser(this.userDetail).toPromise();
  else
  {
	this.userDetail.created_by= this._dbService.getCurrentUserDetail().email;
  this.userDetail.created_time = Date.now(); 

  result = await this._dbService.addUser(this.userDetail).toPromise();
  }
  CustomLogger.logStringWithObject("addUser:result:", result);
  if (!this.isUpdate)
  
      CustomMisc.showAlert("User Added Successfully");
  else
      CustomMisc.showAlert("User Updated Successfully");
  this._router.navigate(["user/userlist"]);
  this.showError = "";
 
} 

catch (error) {
  CustomLogger.logStringWithObject("eeeeeeeeeeeeeeeee", error);
  CustomLogger.logStringWithObject("mmmmmmmmmmmmm", error.error.message);
  CustomLogger.logStringWithObject("kkkkkkkkkk", error['error'].message);
  CustomLogger.logError(error.message);
  CustomMisc.showAlert(error.error.message, true);
  this.showError = error.error.message;
}  
  
}

  
  toggleFieldTextType()
 {
  this.fieldTextType = !this.fieldTextType;
}
/* selectImage(event) {
  if (event.target.files.length > 0) {
    const file = event.target.files[0];
    this.file = file;
  }
}  */



selectImage(event){
/* const file = (event.target as HTMLInputElement).files[0]; */
if (event.target.files.length > 0) {
  const file = event.target.files[0];
  this.file = file;

this.form.patchValue({files: file});

this.form.get('files').updateValueAndValidity()

// File Preview
const reader = new FileReader();
reader.onload = () => {
  this.preview = reader.result as string;
}
reader.readAsDataURL(file)
}
}


onClickForm() {
  this.userDetail.email = "";
  this.userDetail.first_name = "";
  this.userDetail.last_name = "";
  this.userDetail.password = "";
  this.userDetail.phone = "";
  this.userDetail.username = "";
  this.userDetail.usertype_name = "";
}
}
