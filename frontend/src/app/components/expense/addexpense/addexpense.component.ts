import { Component, OnInit } from '@angular/core';
import { DBService } from 'src/app/services/dbservice.service';
import { ExpenseDetail } from 'src/app/models/ExpenseDetail.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ExpenseitemDetail } from 'src/app/models/ExpenseitemDetail';
import { CustomLogger } from '../../../models/utils/CustomLogger';
import { CustomMisc } from '../../../models/utils/CustomMisc';
import { Utility } from 'src/app/services/utility.service';
import { GlobalConstants } from 'src/app/models/utils/GlobalConstants';
import { FormBuilder, FormGroup } from "@angular/forms";
@Component({
  selector: 'app-addexpense',
  templateUrl: './addexpense.component.html',
  styleUrls: ['./addexpense.component.css']
})
export class AddexpenseComponent implements OnInit {

  expenseDetail: ExpenseDetail;
  expenseItemDetail: ExpenseitemDetail[] = [];
  file 
  form: FormGroup;
  
  preview: string;
  constructor(private _dbService: DBService, private _router: Router, private _activatedRoute: ActivatedRoute,private util: Utility, public fb: FormBuilder,) {
    this.form = this.fb.group({
      name: [''],
      files: [null]
    })
   }

  isUpdate = false;


   
  async htmlInit() {

    let result = await this._dbService.getAllExpenseItem().toPromise();
    this.expenseItemDetail = result["data"];
  }
  catch(error) {
    CustomLogger.logStringWithObject("ERROR:", error);
  }

  ngOnInit() {
    this.expenseDetail = new ExpenseDetail();
    this.htmlInit();
    this._activatedRoute.params.subscribe(
      async params => {
        console.log("params:", params);
        let expense_id = params["id"];
        if (expense_id) {
          let result = await this._dbService.getExpense(expense_id).toPromise();
          console.log(result);
          this.expenseDetail = result["data"];
          this.isUpdate = true;
          this.preview=this.expenseDetail.files;
        }

      }
    );

  }



  async onSubmit() {
    CustomLogger.logStringWithObject("Will save expenses...", this.expenseDetail);
    this.expenseDetail.updated_by= this._dbService.getCurrentUserDetail().email; 
    this.expenseDetail.modified_time = Date.now();
	
    try {
      let result = null;
      CustomLogger.logStringWithObject("FILES:", this.file);
      if (this.file != undefined || this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        result = await this._dbService.uploadFile(formData).toPromise();
        CustomLogger.logStringWithObject("uploadFile:result:", result);

        let fileName = GlobalConstants.DEFAULTS_FILE_LOCATIONS.ENTITY_ICONS + result["data"].filename;//received after file added
        CustomLogger.logStringWithObject("files:", fileName);
        this.expenseDetail.files = fileName;
      }
      if (this.isUpdate)
        result = await this._dbService.updateExpense(this.expenseDetail).toPromise();
      else {
        this.expenseDetail.created_by= this._dbService.getCurrentUserDetail().email;
        this.expenseDetail.created_time = Date.now();
        result = await this._dbService.addExpense(this.expenseDetail).toPromise();
      }
      CustomLogger.logStringWithObject("addexpense:result:", result);
      if (!this.isUpdate)
        CustomMisc.showAlert("Expenses Added Successfully");
      else
        CustomMisc.showAlert("Expenses Updated Successfully");
      this._router.navigate(["expense/expenselist"]);

    }
    catch (error) {
      CustomLogger.logError(error);
      CustomMisc.showAlert("Error in adding Expense: " + error.message, true);
    }

  }

  selectImage(event){
    /* const file = (event.target as HTMLInputElement).files[0]; */
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.file = file;
    
    this.form.patchValue({files: file});
    this.form.get('files').updateValueAndValidity()
    
    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.preview = reader.result as string;
    }
    reader.readAsDataURL(file)
    }
    }
  onClickForm() {
    this.expenseDetail.amount = "";
    this.expenseDetail.date = "";
    this.expenseDetail.expense_id = "";
    this.expenseDetail.expense_item = "";
   
}
}
