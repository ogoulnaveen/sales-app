import { Component, OnInit } from '@angular/core';

import { Utility } from 'src/app/services/utility.service';
import { DBService } from 'src/app/services/dbservice.service';
import { UserDetail } from 'src/app/models/UserDetail.model';
import { Router, ActivatedRoute } from '@angular/router';

import { CustomLogger } from '../../../models/utils/CustomLogger';
import { CustomMisc } from '../../../models/utils/CustomMisc';
import { UserType } from 'src/app/models/UserType.model';
import { GlobalConstants } from 'src/app/models/utils/GlobalConstants';


@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
  userDetail: UserDetail;
  userType: UserType[] = [];
  file 
  preview: string;
  constructor(private _dbService: DBService, private _router: Router, private _activatedRoute: ActivatedRoute, private _util: Utility,) { }

  isUpdate = false;
  fieldTextType: boolean;



  async htmlInit() {
    
    let result = await this._dbService.getAllUserType().toPromise();
    this.userType = result["data"];
} 
catch (error) {
    CustomLogger.logStringWithObject("ERROR:", error);
}



async ngOnInit() {
  this.userDetail = this._dbService.getCurrentUserDetail();
  this.isUpdate = false;
  this.preview=this.userDetail.files;

  
}
   

  async onSubmit() {
    CustomLogger.logStringWithObject("Will save user...", this.userDetail);
    try {
            
     let result = await this._dbService.updateUser(this.userDetail).toPromise();
      CustomLogger.logStringWithObject("result:", result);
      CustomMisc.showAlert("User Profile successfully updated...");
  } catch (err)
   {
      CustomLogger.logStringWithObject("error from database:", err.error.message);
      alert("Error:::" + err.error.message);
  }
  
}

   /*  this.userDetail.updated_by= this._dbService.getCurrentUserDetail().email; 
    this.userDetail.modified_time = Date.now();
    try {
      let result = null;
      CustomLogger.logStringWithObject("FILES:", this.file);
      if (this.file != undefined || this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        result = await this._dbService.uploadFile(formData).toPromise();
        CustomLogger.logStringWithObject("uploadFile:result:", result);

        let fileName = GlobalConstants.DEFAULTS_FILE_LOCATIONS.ENTITY_ICONS + result["data"].filename;//received after file added
        CustomLogger.logStringWithObject("files:", fileName);
        this.userDetail.files = fileName;

      }
      if (this.isUpdate)
      result = await this._dbService.updateUser(this.userDetail).toPromise();
  else
  {
	this.userDetail.created_by= this._dbService.getCurrentUserDetail().email;

  this.userDetail.created_time = Date.now();
  result = await this._dbService.addUser(this.userDetail).toPromise();
  }
  CustomLogger.logStringWithObject("addUser:result:", result);
  if (!this.isUpdate)
      CustomMisc.showAlert("User Added Successfully");
  else
      CustomMisc.showAlert("User Updated Successfully");
  this._router.navigate(["user/userlist"]);

} 
catch (error) {
  CustomLogger.logError(error);
  CustomMisc.showAlert("Duplicate username/Email not allowed:" );
}    
 */


 
  
  toggleFieldTextType()
 {
  this.fieldTextType = !this.fieldTextType;
}
selectImage(event) {
  if (event.target.files.length > 0) {
    const file = event.target.files[0];
    this.file = file;
  }
} 
onClickForm() {
  this.userDetail.email = "";
  this.userDetail.first_name = "";
  this.userDetail.last_name = "";
  this.userDetail.password = "";
  this.userDetail.phone = "";
  this.userDetail.username = "";
  this.userDetail.usertype_name = "";
}
}
