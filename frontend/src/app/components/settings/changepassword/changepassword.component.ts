import { Component, OnInit } from '@angular/core';
import { Utility } from 'src/app/services/utility.service';
import { DBService } from 'src/app/services/dbservice.service';
import { UserDetail } from 'src/app/models/UserDetail.model';
import { Router, ActivatedRoute } from '@angular/router';

import { CustomLogger } from '../../../models/utils/CustomLogger';
import { CustomMisc } from '../../../models/utils/CustomMisc';
import { UserType } from 'src/app/models/UserType.model';
import { GlobalConstants } from 'src/app/models/utils/GlobalConstants';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {


  //1 - get the user details of the current user
  // 2 - get new password and confirm password
  //3 - compare the two passwords and if correct then store it in database and log out
  userDetail: UserDetail;
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;

  constructor(private _dbService: DBService, private _router: Router, private _activatedRoute: ActivatedRoute, private _util: Utility,) { }

  async ngOnInit() {
    this.userDetail = this._dbService.getCurrentUserDetail();
  }

  async onSubmit() {

    console.log("oldPassword:", this.oldPassword);
    console.log("newPassword:", this.newPassword);
    if(this.oldPassword != this.userDetail.password){
      console.log("Old Password mismatch");
      alert("Old Password mismatch");
    } else if(this.newPassword != this.confirmPassword) {
      console.log("New password mismatch");      
      alert("New password mismatch");
    } else {
      this.userDetail.password = this.newPassword;
      let result = await this._dbService.updateUser(this.userDetail).toPromise();
        CustomLogger.logStringWithObject("result:", result);
       this._router.navigate(["auth/login"]);
    }
    
  }

}
