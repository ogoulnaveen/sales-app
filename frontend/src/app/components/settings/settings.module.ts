import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { HistoryComponent } from './history/history.component';
import { SettingsRoutingModule } from './settings.routing'
import { DataTableModule } from 'angular-6-datatable';
import { HttpClientModule } from '@angular/common/http';

import {AgmCoreModule, AgmMap, MouseEvent,MapsAPILoader  } from '@agm/core';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
//import {DpDatePickerModule} from 'ng2-date-picker';

@NgModule({
  imports: [
  
    SharedModule,
    SettingsRoutingModule,
    DataTableModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBhFzy3EwbidUdK98BWrSs6lNECgT6h3MM'
    }),
    //DpDatePickerModule
  ],
  declarations: [
    HistoryComponent,
    EditprofileComponent,
    ChangepasswordComponent,
    
  ],
})
export class SettingsModule { }
