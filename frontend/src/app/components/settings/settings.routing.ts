import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryComponent } from './history/history.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'visitmode',
      status: false
    },
    children: [
      {
        path: 'history',
        component: HistoryComponent
      },
      {
        path: 'editprofile',
        component: EditprofileComponent
      },
      {
        path: 'changepassword',
        component: ChangepasswordComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SettingsRoutingModule { }