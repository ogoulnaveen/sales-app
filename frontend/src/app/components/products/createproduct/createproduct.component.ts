import { Component, OnInit } from '@angular/core';
import { DBService } from 'src/app/services/dbservice.service';
import { CategoryDetail } from 'src/app/models/CategoryDetail.model';
import { ProductDetail } from 'src/app/models/ProductDetail.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomLogger } from '../../../models/utils/CustomLogger';
import { CustomMisc } from '../../../models/utils/CustomMisc';
import { GlobalConstants } from 'src/app/models/utils/GlobalConstants';
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-createproduct',
  templateUrl: './createproduct.component.html',
  styleUrls: ['./createproduct.component.css']
})
export class CreateproductComponent  implements OnInit {

  productDetail : ProductDetail;
  categoryDetail: CategoryDetail[] = [];
  file 
  form: FormGroup;
  
  preview: string;
  constructor(private _dbService: DBService, private _router: Router, private _activatedRoute: ActivatedRoute,public fb: FormBuilder,) { 
     // Reactive Form
     this.form = this.fb.group({
      name: [''],
      files: [null]
    })
  }
  isUpdate = false;

  async htmlInit() {
    
    let result = await this._dbService.getAllCategory().toPromise();
    this.categoryDetail = result["data"];
} 
catch (error) {
    CustomLogger.logStringWithObject("ERROR:", error);
}


  ngOnInit() {
    this.productDetail = new ProductDetail();
    this.htmlInit();
    this._activatedRoute.params.subscribe(
      async params => {
        console.log("params:", params);
        let product_id = params["id"];
        if (product_id) {
          let result = await this._dbService.getProduct(product_id).toPromise();
          console.log(result);
          this.productDetail = result["data"];
          this.isUpdate = true;
            this.preview=this.productDetail.files;
        }

      }
    );

  }





  showError = "";
  async onSubmit() {
    CustomLogger.logStringWithObject("Will save product...", this.productDetail);
    this.productDetail.updated_by= this._dbService.getCurrentUserDetail().email; 
    this.productDetail.modified_time = Date.now();
	
    try {
      let result = null;
      CustomLogger.logStringWithObject("FILES:", this.file);
      if (this.file != undefined || this.file != null) {
        const formData = new FormData();
        formData.append('file', this.file);
        result = await this._dbService.uploadFile(formData).toPromise();
        CustomLogger.logStringWithObject("uploadFile:result:", result);

        let fileName = GlobalConstants.DEFAULTS_FILE_LOCATIONS.ENTITY_ICONS + result["data"].filename;//received after file added
        CustomLogger.logStringWithObject("files:", fileName);
        this.productDetail.files = fileName;

      }
      if (this.isUpdate)
      result = await this._dbService.updateProduct(this.productDetail).toPromise();
  else
  {
	this.productDetail.created_by= this._dbService.getCurrentUserDetail().email;

  this.productDetail.created_time = Date.now();
  result = await this._dbService.addProduct(this.productDetail).toPromise();
  }
  CustomLogger.logStringWithObject("addProduct:result:", result);
  if (!this.isUpdate)
      CustomMisc.showAlert("Product Added Successfully");
  else
      CustomMisc.showAlert("Product Updated Successfully");
  this._router.navigate(["products/productlist"]);
  this.showError = "";

} 
catch (error) { 
 /*  CustomLogger.logStringWithObject("eeeeeeeeeeeeeeeee", error);
  CustomLogger.logStringWithObject("mmmmmmmmmmmmm", error.error.message);
  CustomLogger.logStringWithObject("kkkkkkkkkk", error['error'].message);
  CustomLogger.logError(error.message); */
 /*  CustomMisc.showAlert(error.error.message, true); */
  this.showError = error.error.message;
}    

  }
 
selectImage(event){
  /* const file = (event.target as HTMLInputElement).files[0]; */
  if (event.target.files.length > 0) {
    const file = event.target.files[0];
    this.file = file;
  
  this.form.patchValue({files: file});
  this.form.get('files').updateValueAndValidity()
  
  // File Preview
  const reader = new FileReader();
  reader.onload = () => {
    this.preview = reader.result as string;
  }
  reader.readAsDataURL(file)
  }
  }
  onClickForm() {
    this.productDetail.product_name = "";
    this.productDetail.category_name = "";
    this.productDetail.cost = 0;
    this.productDetail.supplier_name = "";
    
 }
}


 /*  ngOnInit() {

    console.log("11111111111");
    this.productDetail = new ProductDetail();

  }

  async onClickSave(){
    console.log("Will save user...", this.productDetail);
    let result = await this._dbService.addProduct(this.productDetail).toPromise();
   
    console.log("Result:", result);
  }}
 */