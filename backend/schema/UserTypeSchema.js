const mongoose = require("mongoose");

const UserTypeSchema = mongoose.Schema({
    usertype_id: { type: String},
   usertype_name: { type: String},
   created_by:{type:String},
   created_time:{type:Number}, 
  updated_by:{type:String},
   modified_time:{type:Number}, 
    
});
module.exports = mongoose.model('UserTypes',  UserTypeSchema, 'user_types');
