const mongoose = require("mongoose");

const ProductDetailSchema = mongoose.Schema({
    product_id: { type: String},
    product_name: { type: String},
    cost: { type: Number},
    supplier_name: { type: String},
    category_name: {type: String},
    files:{type:String},
    created_by:{type:String},
    created_time:{type:Number}, 
   updated_by:{type:String},
    modified_time:{type:Number}, 
});
module.exports = mongoose.model('ProductDetails', ProductDetailSchema, 'product_details');
